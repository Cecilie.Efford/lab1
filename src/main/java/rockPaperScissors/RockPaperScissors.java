package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while(true) {
            System.out.println("Let's play round " + roundCounter +  "\n" +
                    "Your choice (Rock/Paper/Scissors)?");

            String computerChoice = getComputerChoice();
            String humanChoice = userChoice();
            isWinner(humanChoice, computerChoice);

            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice ;

            if(isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + ". Human wins!");
                humanScore++;
            }else if(isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + ". Computer wins!");
                computerScore++;
            }else {
                System.out.println(choiceString + ". Its a tie!");

            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            System.out.println("Do you wish to continue playing? (y/n)?");
            String answer = sc.nextLine();

            if(answer.equals("y")) {
            continue;
            }else if(answer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

            }
        roundCounter++;
        }



        // sjekker om input er valid(true)
    boolean isValidChoice(String humanChoice, List<String> rpsChoices) {
        String input = humanChoice.toLowerCase();
        return rpsChoices.contains(humanChoice);

    }
// får brukerinput og sjekker om det er valid ved å bruke isValidChoice. hvis det er valid, returnerer den
// inputtet, om ikke spør den om nytt input.
    String userChoice() {
        while (true) {
            String humanChoice = sc.nextLine();
            if (isValidChoice(humanChoice, rpsChoices)) {
                return humanChoice;
            } else {   System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }

        }
    }



    private String getComputerChoice() {
        Random random = new Random();
        return rpsChoices.get(random.nextInt(0, rpsChoices.size()));

    }
    boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")){ // blir true om choice1 vinner over choice2
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }

    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
